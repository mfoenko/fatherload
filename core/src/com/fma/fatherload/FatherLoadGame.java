package com.fma.fatherload;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fma.fatherload.game.FatherLoadController;

public class FatherLoadGame extends ApplicationAdapter {
	SpriteBatch batch;
	FatherLoadController gameController;

	@Override
	public void create () {
		batch = new SpriteBatch();
		gameController = new FatherLoadController();
	}

	@Override
	public void render () {
		gameController.preDraw(batch);

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		gameController.draw(batch);
		batch.end();

		gameController.timeStep(Gdx.graphics.getDeltaTime());
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		gameController.viewport.update(width,height);
	}
}
