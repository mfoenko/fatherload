package com.fma.fatherload.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by mikhail on 12/23/17.
 */
public class FatherLoadController implements ContactListener {

    public World world;
    public Player player;

    public Box2DDebugRenderer renderer;

    private static final float CAMERA_MAX_VEL = 1;
    private static final int WORLD_WIDTH = 50;
    public OrthographicCamera camera;
    public ScreenViewport viewport;
    private final Map map;

    public FatherLoadController() {
        world = new World(new Vector2(0, -10), true);
        player = new Player(world, new Vector2(1, -25));

        camera = new OrthographicCamera();
        camera.setToOrtho(false);
        viewport = new ScreenViewport(camera);
        camera.zoom = .02f;
        camera.position.set(0, 0, 1);
        renderer = new Box2DDebugRenderer();

        world.setContactListener(this);

        map = new Map(WORLD_WIDTH, 500);
        map.generateInteractableTiles(player);
        createTileBodies(map);

//        world.c

//        testMakeFloor();
    }


    public void timeStep(float timeStep) {
//        renderer.render(world, camera.combined);
        Vector2 cameraMovement = new Vector2(player.body.getWorldCenter().x, player.body.getWorldCenter().y);
        cameraMovement.sub(camera.position.x, camera.position.y);
        cameraMovement.clamp(0, CAMERA_MAX_VEL);
        camera.position.add(new Vector3(cameraMovement.x, cameraMovement.y, 0));

        world.step(1 / 45f, 7, 7);
        player.timeStep(timeStep);
        updateMapBodies();
    }

    public void preDraw(SpriteBatch batch) {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        System.out.println(Gdx.graphics.getFramesPerSecond());
        Vector2 playerPosition = player.body.getPosition();
        if(playerPosition.x > WORLD_WIDTH){
            player.body.setTransform(playerPosition.x-WORLD_WIDTH,playerPosition.y,player.body.getAngle());
            camera.position.set(camera.position.x-WORLD_WIDTH, camera.position.y,camera.position.z);
        }else if(playerPosition.x < 0){
            player.body.setTransform(playerPosition.x+WORLD_WIDTH,playerPosition.y,player.body.getAngle());
            camera.position.set(camera.position.x+WORLD_WIDTH, camera.position.y,camera.position.z);
        }

    }

    public void draw(SpriteBatch batch) {
        player.draw(batch);
        for(int i=tilesLoadedStart;i<tilesLoadedEnd+1;i++){
            for(int x=0;x<map.getxSize();x++){
                map.getInteractableTile(x,i).draw(batch, WORLD_WIDTH);
            }
        }

    }

    private void testMakeFloor() {
        BodyDef bDef = new BodyDef();
        bDef.type = BodyDef.BodyType.StaticBody;
        bDef.position.set(0, -5);
        Body floor = world.createBody(bDef);

        FixtureDef fDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(10f, .5f);
        fDef.shape = shape;
        floor.createFixture(fDef);
    }

    @Override
    public void beginContact(Contact contact) {
        if (contact.getFixtureA().getBody() == player.body && contact.getFixtureB().getBody().getType() == BodyDef.BodyType.StaticBody
                || contact.getFixtureB().getBody() == player.body && contact.getFixtureA().getBody().getType() == BodyDef.BodyType.StaticBody
                ) {
            player.onGround();
        }
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    private int tilesLoadedStart;
    private int tilesLoadedEnd;
    private static final int MIN_BODY_BUFFER = 15;
    private static final int MAX_BODY_BUFFER = 25;


    public void updateMapBodies() {
        int playerY = (int) -player.body.getPosition().y;

        if (tilesLoadedEnd - playerY <= MIN_BODY_BUFFER) {
            moveLoadedTilesDown(MAX_BODY_BUFFER-MIN_BODY_BUFFER);
        } else if (playerY - tilesLoadedStart <= MIN_BODY_BUFFER) {
            moveLoadedTilesUp(MAX_BODY_BUFFER-MIN_BODY_BUFFER);
        }
    }

    private void moveLoadedTilesDown(int amount) {
        for (int i = tilesLoadedStart; i < tilesLoadedStart + amount; i++) {
            for (int x = 0; x < map.getxSize(); x++) {
                InteractableTile tile = map.getInteractableTile(x, i);
                if (tile != null && tile.body != null) {
                    world.destroyBody(tile.body);
                }
            }
        }

        tilesLoadedStart += amount;

        for (int y = tilesLoadedEnd; y < tilesLoadedEnd + amount; y++) {
            for (int x = 0; x < map.getxSize(); x++) {
                InteractableTile tile = map.getInteractableTile(x, y);
                if (tile != null) {
                    tile.createBody(world);
                }
                map.getInteractableTile(0,y).createBody(world,WORLD_WIDTH,y);
                map.getInteractableTile(WORLD_WIDTH-1,y).createBody(world,-1,y);
            }
        }
        tilesLoadedEnd += amount;
    }

    private void moveLoadedTilesUp(int amount) {

    }

    public void createTileBodies(Map map) {
        int playerY = -(int) player.body.getPosition().y;
        tilesLoadedStart = playerY - MAX_BODY_BUFFER;
        if (tilesLoadedStart < 0) {
            tilesLoadedStart = 0;
        }

        tilesLoadedEnd = playerY + MAX_BODY_BUFFER;
        if (tilesLoadedEnd > map.getySize()) {
            tilesLoadedEnd = map.getySize();
        }

        for (int y = tilesLoadedStart; y < tilesLoadedEnd; y++) {
            for (int x = 0; x < map.getxSize(); x++) {
                map.getInteractableTile(x, y).createBody(world);
            }
            map.getInteractableTile(0,y).createBody(world,WORLD_WIDTH,y);
            map.getInteractableTile(WORLD_WIDTH-1,y).createBody(world,-1,y);
        }

    }

}
