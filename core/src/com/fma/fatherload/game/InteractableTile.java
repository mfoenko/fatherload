package com.fma.fatherload.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by james on 12/23/17.
 */
public class InteractableTile {
    private int health;
    private int friction;
    private int value;
    private int xpos;
    private int ypos;

    private static Texture stoneTexture = new Texture(Gdx.files.internal("stone.png"));

    private String name;

    public Body body;

    //private texture tiletexture;

    public InteractableTile(int health, int friction, int value, int xpos, int ypos, String name/*, texture tileTexture*/) {
        this.health = health;
        this.friction = friction;
        this.value = value;
        this.xpos = xpos;
        this.ypos = ypos;
        this.name = name;
        //this.tileTexture = tileTexture;
    }

    public int getHealth() {
        return health;
    }

    public int getFriction() {
        return friction;
    }

    public int getValue() {
        return value;
    }

    public int getXpos() {
        return xpos;
    }

    public int getYpos() {
        return ypos;
    }

    public String getName() {
        return name;
    }
    //public texture getTexture(){return tileTexture;}

    public void setHealth(int newHealth) {
        health = newHealth;
    }
    //public void setTexture(int newTexture){tileTexture = newTexture;}

    public static InteractableTile chooseTile(int x, int y, double spawnThreshold) {
        double selection = Math.random();
        selection *= 100;
        if (selection <= spawnThreshold) {
            switch ((int) (Math.random() * 2)) {
                case 1:
                    return new InteractableTile(1, 1, 1, x, y, "Rock");
                default:
                    return new InteractableTile(1, 1, 0, x, y, "Dirt");

                /*int selection = Math.random() % Zone.getTileSpace();
                InteractableTile tile = Zone.possibleTiles[selection];
                tile.setxPos(x);
                tile.setyPos(y);
                return tile;*/
            }
        }
        return new InteractableTile(0, 0, 0, x, y, "Air");
    }

    public static Boolean equals(InteractableTile newtile, InteractableTile oldtile) {
        Boolean identical = true;
        if (newtile.getXpos() != oldtile.getXpos())
            identical = false;
        if (newtile.getYpos() != oldtile.getYpos())
            identical = false;
        if (newtile.getHealth() != oldtile.getHealth())
            identical = false;
        if (newtile.getFriction() != oldtile.getFriction())
            identical = false;
        if (newtile.getName() != oldtile.getName())
            identical = false;
        return identical;
    }

    public void createBody(World world){
        createBody(world, xpos,ypos);
    }

    public void createBody(World world, int x, int y) {
        if (!"Air".equals(name)) {
            BodyDef bDef = new BodyDef();
            bDef.awake = false;
            bDef.type = BodyDef.BodyType.StaticBody;
            bDef.position.set(x, -y);
            Body tileBody = world.createBody(bDef);

            FixtureDef fDef = new FixtureDef();
            PolygonShape shape = new PolygonShape();
            shape.setAsBox(.5f, .5f);
            fDef.shape = shape;
            tileBody.createFixture(fDef);
            shape.dispose();

            body = tileBody;
        }
    }

    public void draw(SpriteBatch batch, int wrap) {
        if (!getName().equals("Air")) {
            batch.draw(stoneTexture, xpos - .5f, -ypos - .5f, 1, 1);
            batch.draw(stoneTexture, xpos - .5f+wrap, -ypos - .5f, 1, 1);
            batch.draw(stoneTexture, xpos - .5f-wrap, -ypos - .5f, 1, 1);
        }
    }
}
