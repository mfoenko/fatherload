package com.fma.fatherload.game;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

/**
 * Created by james on 12/23/17.
 */
public class Map {
    private int xSize;
    private int ySize;
    //private Array<BackgroundTile[]> BackgroundTiles;
    private Array<InteractableTile[]> interactableTiles = new Array<InteractableTile[]>();
    //private Array<ForegroundTile[]> ForegroundTiles;

    private static final int MIN_TILES_ABOVE_BELOW_PLAYER = 15;
    private int offset = 0; //what world row does row 0 of the tile arrays corresponds to

        /*private Array[InteractableTile] tile1array;
        private Array[FatherLoadInteractibleTile] tile2array;
        private Array[FatherLoadInteractibleTile] tilenarray;*/

    public Map(int xSize, int ySize) {
        this.xSize = xSize;
        this.ySize = ySize;
    }

    public int getxSize() {
        return xSize;
    }

    public int getySize() {
        return ySize;
    }

    public InteractableTile getInteractableTile(int x, int y) {
        InteractableTile[] line = interactableTiles.get(y);
        return line[x];
    }


    public void setInteractableTile(int x, int y, InteractableTile tile) {
        InteractableTile[] line = interactableTiles.get(y);
        line[x] = tile;
    }
    //BackgroundTile getBackgroundTile(int x, int y){return BackgroundTiles[x][y];}
    //ForegroundTile getForegroundTile(int x, int y){return ForegroundTiles[x][y];}

    public void generateInteractableTiles(Player player) {
        generateInitialMap();
        generatePlayerSpawn(player.body.getPosition().x, player.body.getPosition().y);
    }

    public void generateInitialMap() {
        InteractableTile newtile;
        for (int y = 0; y < ySize; y++) {
            interactableTiles.add(new InteractableTile[xSize]);
            for (int x = 0; x < xSize; x++) {
                newtile = InteractableTile.chooseTile(x, y, 60);
                setInteractableTile(x, y, newtile);
                //recordTypeAndLocation(tile);
            }
        }
    }

    public void generatePlayerSpawn(float playerx, float playery) {
        int intx = (int) (playerx);
        int inty = (int) -(playery);
        for (int x = intx - 1; x <= intx + 1; x++) {
            for (int y = inty - 1; y <= inty + 1; y++) {
                setInteractableTile(x, y, new InteractableTile(0, 0, 0, x, y, "Air"));
            }
        }
    }

    private boolean isUpdating = false;

    public void updateMap(int playerYPos, World world) {
        if (!isUpdating &&
                (getySize() - (playerYPos - offset) <= MIN_TILES_ABOVE_BELOW_PLAYER
                        || playerYPos - offset <= MIN_TILES_ABOVE_BELOW_PLAYER)) {

                loadMap(15, getySize() - (playerYPos - offset) <= MIN_TILES_ABOVE_BELOW_PLAYER, world);
        }
    }

    public void loadMap(int numAdd, boolean addToBottom, World world) {
        if (addToBottom) {
            loadMapDown(numAdd, world);
        } else {
            loadMapUp(numAdd, world);
        }
    }

    private void loadMapUp(int numAdd, World world) {
        //unload bottom tiles
        for (int i = ySize - 1; i >= ySize - numAdd; i++) {
            for (int x = 0; x < xSize; x++) {
                world.destroyBody(interactableTiles.get(i)[x].body);
            }
        }

        //make tiles for top
        Array<InteractableTile[]> newTiles = generateTiles(xSize, numAdd);

        //remove bottom tiles
        interactableTiles.removeRange(xSize - numAdd, xSize - 1);

        //add top tiles
        newTiles.addAll(interactableTiles);
        interactableTiles = newTiles;


        offset += numAdd;
    }

    private void loadMapDown(int numAdd, World world) {
        //unload from top
        for (int i = 0; i < numAdd; i++) {
            for (int x = 0; x < xSize; x++) {
                world.destroyBody(interactableTiles.get(i)[x].body);
            }
        }


        //create tiles for bottom
        Array<InteractableTile[]> newTiles = generateTiles(xSize, numAdd);

        //remove from top
        interactableTiles.removeRange(0, numAdd - 1);
        //add to bottom
        interactableTiles.addAll(newTiles);

        offset += numAdd;

    }

    public  Array<InteractableTile[]> generateTiles(int width, int height){
        Array<InteractableTile[]> tiles = new Array<InteractableTile[]>();
        for(int y=0;y<height;y++){
            tiles.add(new InteractableTile[width]);
            for(int x=0;x<width;x++){
                setInteractableTile(x, y, InteractableTile.chooseTile(x, y, 60));
            }
        }
        return tiles;
    }


        /*void recordTypeAndLocation(tile){
            switch(tile.getName()){
                case tile1.getName():
                    tile1array.add(tile);
                    break;
                case tile2.getName():
                    tile2array.add(tile);
                    break;
                case tilen.getName():
                    tilenarray.add(tile);
                    break;
            }
        }*/

}
