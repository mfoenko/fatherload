package com.fma.fatherload.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by mikhail on 12/24/17.
 */
public class Player {

    public Body body;
    static Texture texture;
    public Sprite sprite;

    static {
        texture = new Texture(Gdx.files.internal("libgdx.jpeg"));

    }

    public Player(World world, Vector2 position) {
        BodyDef bDef = new BodyDef();
        bDef.type = BodyDef.BodyType.DynamicBody;
        bDef.position.set(position);
        body = world.createBody(bDef);


        FixtureDef fDef = new FixtureDef();
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(.4f, .4f);
        fDef.shape = polygonShape;
        fDef.density = 15;

        body.createFixture(fDef);

        sprite = new Sprite(texture);
        sprite.setSize(.8f, .8f);
        sprite.setOriginCenter();

        body.setAngularDamping(100);

        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {

            }

            @Override
            public void endContact(Contact contact) {

            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });

    }


    public void draw(SpriteBatch batch) {
        sprite.setPosition(body.getPosition().x-.4f, body.getPosition().y - .4f);
        sprite.setRotation((float)(body.getAngle()*180/Math.PI));
        sprite.draw(batch);
    }

    private static final Vector2 LEFT = new Vector2(-1, 0);
    private static final Vector2 RIGHT = new Vector2(1, 0);
    private static final Vector2 UP = new Vector2(0, 1);
    private static final Vector2 DOWN = new Vector2(0, -1);


    private float jumpTimer = 0;
    private boolean canJump = false;
    private boolean isJumping = false;
    private static final float MAX_JUMP_DURATION = .25f;
    private static final float MAX_JUMP_VEL = 5;
    private static final float MIN_JUMP_DURATION = 0;
    private static final float MIN_JUMP_VEL = 3;


    private static final float JUMP_VEL_SLOPE = (MAX_JUMP_VEL-MIN_JUMP_VEL)/(MAX_JUMP_DURATION-MIN_JUMP_DURATION);
    private static final float JUMP_VEL_0 = 3;

    public void onGround() {
        canJump = true;
        jumpTimer = 0;
    }

    public void timeStep(float delta) {

        Vector2 movement = new Vector2();

        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            movement.add(LEFT);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            movement.add(RIGHT);
        }

        if (Math.abs(body.getLinearVelocity().x) < 4) {
            body.setLinearVelocity(body.getLinearVelocity().add(movement));
            if (body.getLinearVelocity().x > 2) {
                body.setLinearVelocity(2, body.getLinearVelocity().y);
            }
            if (body.getLinearVelocity().x < -2) {
                body.setLinearVelocity(-2, body.getLinearVelocity().y);
            }
        }


        if ((isJumping || canJump) && Gdx.input.isKeyPressed(Input.Keys.W)) {
            body.setLinearVelocity(body.getLinearVelocity().x, jumpTimer*JUMP_VEL_SLOPE+JUMP_VEL_0);
            canJump = false;
            jumpTimer += delta;
            if (jumpTimer > MAX_JUMP_DURATION) {
                isJumping = false;
                canJump = false;
            } else {
                isJumping = true;
            }
        }else if(isJumping && !Gdx.input.isKeyPressed(Input.Keys.W)){
            isJumping = false;
        }


    }


}
